<?php
class Sorteio extends ConnDB
{
    private $query;

    private function prepExec($prep,$exec)
    {
        $this -> query = $this -> getConn() -> prepare($prep);
        $this -> query -> execute($exec);
    }

    public function insertSorteio($prep,$exec)
    {
        $this -> prepExec('INSERT INTO tbl_sorteios SET '. $prep .'', $exec);
        return $this -> query;
    }

    public function selectSorteio($campos,$prep,$exec)
    {
        $this -> prepExec('SELECT '. $campos .' FROM tbl_sorteios '. $prep .'', $exec);
        return $this -> query;
    }
}
