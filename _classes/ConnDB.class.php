<?php
abstract class ConnDB
{
    private static $conn;

    private function setConn()
    {
        return
            is_null(self::$conn) ?
                self::$conn=new PDO('mysql:host=localhost;dbname=', 'user', 'password') :
                self::$conn;

    }

    public function getConn()
    {
        return $this->setConn();
    }
}
