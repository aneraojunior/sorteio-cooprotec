<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

    function __autoload($class){require_once"_classes/{$class}.class.php";}

    sleep(2);

    $sorteio = new Sorteio;
    $sel_sorteio = $sorteio -> selectSorteio('*', 'WHERE id > ? order by RAND() limit 1', array(0));
    $row_sorteio = $sel_sorteio -> fetch();
?>

<br>
<div class="alert alert-success">
    <h4><span style="color: #777777; " align="center">
            Sorteio realizado com sucesso <br><br>
            Sorteado: <strong><?php echo utf8_encode($row_sorteio['nome']); ?></strong> <br>
            Código do sorteado: <strong><?php echo $row_sorteio['id']; ?></strong>
    </span></h4>
</div>

