<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sorteio Cooprotec</title>

    <!-- CSS -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,700">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/media-queries.css">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script> <!-- Script para funcionar o insert sem refresh -->
    <script type="text/javascript" src="http://digitalbush.com/wp-content/uploads/2013/01/jquery.maskedinput-1.3.1.min_.js"></script>

    <script type="text/javascript" language="javascript">
        $(function($) {
            $("#formulario").submit(function() {
                $("#status").html("<br><img src='loader3.gif' width='20%' />");
                $.post('result.php', function(resposta) {
                    $("#status").slideDown();
                    if (resposta != false) {
                        $("#status").html(resposta);
                    }
                });
            });
        });
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1741081422790087'); // Insert your pixel ID here.
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1741081422790087&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Top menu -->
<nav class="navbar navbar-inverse navbar-fixed-top navbar-no-bg" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img src="assets/img/logo_grande_h.png" width="150" height="40" style="margin-top: 4px;">
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="top-navbar-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a class="scroll-link" href="#top-content">Home</a></li>
                <li><a class="scroll-link" href="#regras">Regras do sorteio</a></li>
                <li><a class="btn btn-link-3 scroll-link" href="#inscricao">Inscreva-se</a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- Top content -->
<div class="top-content">
    <div class="container">
        <div class="row">

            <div class="col-md-2"></div>
            <div class="col-sm-8 wow fadeInLeft">
                <h1><span style="color: white; ">Sorteio de Identidade Visual</span> </h1>
                <div class="description">
                    <p class="medium-paragraph">
                        <span style="color: white; ">
                            A <b>Cooprotec</b> está sorteando uma Identidade Visual para seu negócio ou empresa. <br> O sorteio será realizado dia 17/02/2017
                        </span>
                    </p>
                </div>
                <div class="top-buttons">
                    <a class="btn btn-link-1 scroll-link" href="#regras">Veja como participar <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="col-md-2"></div>

            <div class="c-form-1-box">

            </div>

        </div>
    </div>
</div>

<!-- Features -->
<div class="regras-container section-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 features section-description wow fadeIn">
                <h2>Sobre a premiação</h2>
                <div class="divider-1"><div class="line"></div></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 features-box wow fadeInUp">
                <div class="features-box-icon"><i class="fa fa-asterisk"></i></div>
                <br>
                <p>O ganhador receberá, por conta da <b>Cooprotec</b>, a identidade visual do seu negócio ou empresa</p>
            </div>
            <div class="col-sm-4 features-box wow fadeInDown">
                <div class="features-box-icon"><i class="fa fa-asterisk"></i></div>
                <br>
                <p>Um logo para sua empresa está incluso no sorteio e a criação da sua fã-page no Facebook</p>
            </div>
            <div class="col-sm-4 features-box wow fadeInUp">
                <div class="features-box-icon"><i class="fa fa-asterisk"></i></div>
                <br>
                <p>Você receberá 1.000 cartões de visitas e um e-mail profissional, como por exemplo: <b>seu_nome@suaempresa.com.br</b></p>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 features section-description wow fadeIn">
                <h2>Informações sobre o sorteio</h2>
                <div class="divider-1"><div class="line"></div></div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 features-box wow fadeInDown">
                <div class="features-box-icon"><i class="fa fa-asterisk"></i></div>
                <br>
                <p>
                    O domínio do seu e-mail tem validade de 1 ano (determinado pela entidade responsável no Brasil). Após este período, fica por sua responsabilidade renovar ou não o domínio junto ao
                    <a href="http://registro.br" target="_blank">registro.br</a>
                </p>
            </div>
            <div class="col-sm-4 features-box wow fadeInUp">
                <div class="features-box-icon"><i class="fa fa-asterisk"></i></div>
                <br>
                <p>Após o sorteio, levaremos cerca de 40 dias para cuidar da sua identidade visual, logo, criação dos cartões e criação do seu e-mail profissional</p>
            </div>
            <div class="col-sm-4 features-box wow fadeInDown">
                <div class="features-box-icon"><i class="fa fa-asterisk"></i></div>
                <br>
                <p>Para ser sorteado, você deve preencher o <a class="scroll-link" href="#inscricao">formulário abaixo</a> com seu nome, e-mail e telefone (telefones repetidos não serão válidos para o sorteio).</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 section-bottom-button wow fadeInUp">
                <a class="btn btn-link-1 scroll-link" href="#inscricao">Inscreva-se <i class="fa fa-angle-right"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- Testimonials -->
<div class="testimonials-container inscricao-container section-container section-container-image-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 testimonials section-description wow fadeIn">
                <h2>Sorteio de Identidade Visual</h2>
                <div class="divider-1"><div class="line"></div></div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="c-form-1-top">
                    <div class="c-form-1-top-left">
                        <h3>Sorteio aleatório</h3>
                    </div>
                    <div class="c-form-1-top-right">
                        <img src="assets/img/logo_grande_h.png">
                    </div>
                </div>
                <div class="c-form-1-bottom">
                    <form id="formulario" action="javascript:func()" method="post">

                        <button type="submit" name="enviar" class="btn btn-link-1 btn-block"><span class="fa fa-refresh"></span> SORTEAR</button>
                    </form>
                </div>
                <div id="status"></div>
            </div>
            <div class="col-md-3"></div>

        </div>
    </div>
    <div style="margin-top: 200px;"></div>
</div>

<!-- Javascript -->
<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.backstretch.min.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/retina-1.1.0.min.js"></script>
<script src="assets/js/waypoints.min.js"></script>
<script src="assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>